import React, {Suspense, lazy} from 'react'
import Teams from './components/Teams'
import Faq from './components/Faq'
import Home from './components/Home'
import Loader from './components/Loader'

const Feed = lazy(() => import('./components/Feed'));
const routes = {
  "/": () => <Home />,
  "/teams": () => <Teams />,
  "/faq": () => <Faq />,
  "/feed": () => <Suspense fallback={<Loader />}><Feed /></Suspense>
};
export default routes;