import React from 'react';
import {FooterWrapper} from '../style'

const Footer = () => {
    return (
        <FooterWrapper>
            <li>Made with <span role="img" aria-label="heart face">😍</span> by Sisense in 2019</li>
            <li>Contact us: <a href="mailto:hackathon@sisense.com">hackathon@sisense.com</a></li>
        </FooterWrapper>
    );
};

export default Footer;