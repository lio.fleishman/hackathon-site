import React from 'react';
import { TwitterTimelineEmbed } from 'react-twitter-embed';
import 'styled-components/macro'

const Feed = () => {
    return (
        <div>
            <TwitterTimelineEmbed
                css={'height:100vh'}
                sourceType="URL"
                url="https://twitter.com/hashtag/sisense_hackathon?lang=en"
                screenName="SisenseH"
                autoHeight
                options={{height: 500}}
            />
        </div>
    );
};

export default Feed;