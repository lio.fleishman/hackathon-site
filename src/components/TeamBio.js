import React, {Suspense, lazy} from 'react';
import {TeamWrapper} from '../style'
import capitalize from '../utils'
import Loader from './Loader'
const LazyImage = lazy(() => import('./LazyImage'));

const TeamBio = ({team}) => {

    return (
        <TeamWrapper>
            <Suspense fallback={<Loader/>}>
                <LazyImage src={team.imgurl} alt="team memebers"/>
            </Suspense>
        <div>
            <h3>Team Name:</h3>
            <p>{team.name}</p>
            <h3>Members:</h3>
            <ul>
                {team.members.map((person)=> {
                    return <li key={person}>{capitalize(person)}</li>
                })}
            </ul>
            <h3>Country: </h3>
            <p>{team.country}</p>
            <h3>Idea: </h3>
            <span>{team.idea}</span>
        </div>
        </TeamWrapper>
    );
};

export default TeamBio;