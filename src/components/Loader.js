import React from 'react';
import 'styled-components/macro'

const Loader = () => {
    return (
        <svg css={'display:block; margin:auto;'} width="50px"  height="50px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" className="lds-rolling">
            <circle cx="50" cy="50" fill="none" stroke="#1d0e0b" strokeWidth="10" r="35" strokeDasharray="164.93361431346415 56.97787143782138" transform="rotate(183.279 50 50)">
                <animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="2.6s" begin="0s" repeatCount="indefinite"></animateTransform>
            </circle>
        </svg>
    );
};

export default Loader;