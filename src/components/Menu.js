import React, {useState} from 'react'
import { useRoutes, A } from 'hookrouter'
import routes from '../Routes'
import {MenuWrapper, RoutesContent} from '../style'
import NoPageFound from './NoPageFound';
import capitalize from '../utils'

const Menu = ()=> {
    const routeResult = useRoutes(routes);
    const [activeRoute, setRoute] = useState(null)
    const RouteNames = ['home','teams','faq','feed']
    return (
    <div>
        <MenuWrapper>
            {RouteNames.map(route => {
             return <A  key={route} 
                        className={activeRoute === route? 'active':''} 
                        onClick={()=> {setRoute(route)} } 
                        href={route === 'home'? '/' : `/${route}`}>
                        {capitalize(route)}
                    </A>   
            })}
        </MenuWrapper>
        <RoutesContent route={activeRoute}>
            {routeResult || <NoPageFound />}
        </RoutesContent>
    </div>
    );
}

export default Menu;