import React from 'react'
import {ImgWrapper} from '../style'

const LazyImage = ({src, alt})=> {
    return (
        <ImgWrapper src={src} alt={alt}/>
    )
}

export default LazyImage