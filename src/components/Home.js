import React from 'react';
import {HomeWrapper} from '../style'

const Home = () => {
    return (
        <HomeWrapper>
            There is no place like home...
        </HomeWrapper>
    );
};

export default Home;