import React from 'react';
import TeamBio from './TeamBio'

const Teams = () => {
    let teams = [{
        imgurl:'https://sisense-hackathon-2019.s3.amazonaws.com/team1.jpg',
        name: 'team1',
        members:['gaby','fofo','miliki'],
        idea: 'wonderfull idea here',
        country: 'Israel'
    },{
        imgurl:'https://sisense-hackathon-2019.s3.amazonaws.com/team1.jpg',
        name: 'team2',
        members:['jhon','charlie','bob'],
        idea: 'wonderfull idea here',
        country: 'Japan'
    }]
    return (
        <div>
            {teams.map((team)=> {
                return <TeamBio key={team.name} team={team}/>
            })}
        </div>
    );
};

export default Teams;