import React from 'react';

const NoPageFound = ()=> {
    return (
      <div>
          This is not the page you are looking for...
      </div>
    );
}

export default NoPageFound;
