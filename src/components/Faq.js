import React from 'react';
import {FaqContainer} from '../style'

const Faq = () => {
    return (
    <FaqContainer>
        <h1>Hackathon faq</h1>

        <h3>What is the theme of this year’s Hackathon?</h3>

        <p>This year the theme is Cloudcathon, Sisense in the cloud, just use cloud tools to solve any problem you want, the sky's the limit, literally !!!
        Do I need to know how to code?
        No, to participate in the hackathon there is no need to know how to code, you can solve any problem you can think about, and is not necessary has to be in the product.
        </p>
        <h3>Do I need a tech lead on my team?</h3>

        <p>No, the teams don’t need a tech lead because the projects don’t need to be necessarily code, or technology.
        How many people can be on the same team?
        The teams will be formed from 1 to 4 Sisense employees, we are not allowing teams greater than 4 and this is not negotiable.
        </p>

        <h3>For how long can i work on the project?</h3>
        <p>The projects will start in the morning of the day stipulated to start the Hackathon (and not a minute before) any code or project should start and end in the hackathon hours and the teams will have the offices to work on the project for as long as they want until 7AM the day after the event started.
        </p>
        <h3>Should i bring food ?</h3>

        <p>No, Sisense will provide food for all the teams during the Hackathon time.</p>

        <h3>We have to stay till the day after?</h3>

        <p>No, the competition is not about who stays longer hours, but the teams that need to stay for long hours or even till the morning will be accompanied by someone from the organizers to help them if necessary.</p>

        <h3>I need to use AWS products with no exception?</h3>

        <p>No, you don’t have to use AWS products, but we encourage you to do it and learn this incredible technologies to enrich your own knowledge and tool set, besides that they provide us with free training that you can start using right now clicking this link or here 
        </p>
        <h3>How will teams be judged?</h3>

        <p>Teams will be judged on: 
        Best Idea - points from 1 to 9
        Is actually solving the problem? points from 1 to 9
        Is ready to use? points from 1 to 9
        Is the solution durable? points from 1 to 9
        Did the whole group participate? points from 1 to 9
        So… what's the prize?

        There will be a regional champion with a smaller prize TBD and a global champion will get a flight ticket to anywhere in the world*

        * anywhere in the world limited to 1500 usd per/person and only in teams with 4 members or less.
        </p>
        <h3>I have a problem, who can i contact?</h3>

        <p>Contact us here: <a href="mailto:hackathon@sisense.com">hackathon@sisense.com</a>
        <br/>
        Slack channel: will be 2 slack channels: 1 for questions and the second one will be to be used as a life feed during the event.
        </p>
        <h3>I finish my project, now what?</h3>

        <p>Every team when finished should make a video, no larger than 5 minutes presenting the project, be original, you can do anything you want!! And send it to: hackathon@sisense.com 
        If there are more files, zip them all together in 1 file and rename the file with the name of the team and country.
        Once you send the files you have an hour until the video will be published on the slack channel for everybody to see.
        </p>
        <h3>Ok where can i sign in, this is great!!</h3>

        <p>Click on the link to subscribe your team, important to do it in the tab of your country.</p>
</FaqContainer>
    );
};

export default Faq;