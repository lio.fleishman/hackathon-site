import React from 'react';
import logo from './sisense_logo.png';
import './App.scss';
import 'styled-components/macro'
import {AppHeader} from '../src/style';
import Menu from './components/Menu';
import Footer from './components/Footer';

const App = ()=> {
    return (
      <div className="App">
        <AppHeader>
          <img src={logo} className="App-logo" alt="sisense logo" />
          <h2 css={'color:#000'}>Sisense Hackathon 2019</h2>
          <img css={'width:90px; max-height:80px;'} src="https://sisense-hackathon-2019.s3.amazonaws.com/aws.png" alt="aws logo"/>
        </AppHeader>
        <Menu/>
        <Footer/>
      </div>
    );
}

export default App;
