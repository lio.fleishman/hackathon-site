import styled from 'styled-components';

const Paragraph = styled.p`
font-size: 18px;
font-family: Arial, Helvetica, sans-serif;
color:purple;
`;

const AppHeader = styled.div`
    display: flex;
    justify-content: space-around;
    background-color: #ffcb00;
    padding: 20px;
`;

const MenuWrapper = styled.ul`
    padding:0;
    display:inline-flex;
a {
    margin: 0 10px;
    text-decoration:none;
    border-bottom: 2px solid white;
    &.active {
        color:#000;
        border-bottom: 2px solid black;
    }
    &:hover {
        border-bottom: 2px solid black;
    }
    &:visited {
        color:#000;
    }
}
`;

const FaqContainer = styled.div`
    text-align: left;
    margin: 20px;
    max-width: 1000px;
    p {
        line-height: 1.5em;
        font-family: sans-serif, Arial, monospace;
        font-weight: 100;
    }
`;

const FooterWrapper = styled.ul`
    background-color: #000;
    color: #fff;
    margin:0;
    padding: 40px;
    list-style-type:none;
    @media (max-width: 600px){
        li {
            text-align: center;
        } 
    }
    @media (min-width: 600px){
        li {
            text-align: left;
        } 
    }
    a {
        color: #fff;
        font-weight:100;
    }
`;

const RoutesContent = styled.div`
    min-height: calc(100vh - 407px);
    padding:0;
    
    display: flex;
    align-items: center;
    justify-content: center;
`;

const HomeWrapper = styled.div`
    padding: 0;
`;

const TeamWrapper = styled.div`
    display:flex;
    flex-direction: row;
    @media (max-width: 1100px){
        flex-direction: column;
    }
    justify-content: center;
    padding:20px;
    div {
        margin: 20px;
        display: flex;
        flex-direction: column;
        align-items: baseline;

        ul {
            list-style-type:none;
            padding:0;
            text-align: left;
            li {
                padding:5px;
            }
        }
    }
`;

const ImgWrapper = styled.img`
    height: auto;
    max-width:100%;
    min-width:350px;
`;

export {Paragraph, MenuWrapper, FaqContainer, AppHeader, FooterWrapper, RoutesContent, HomeWrapper, TeamWrapper, ImgWrapper};